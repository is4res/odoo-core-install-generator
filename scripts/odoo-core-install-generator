#!/usr/bin/python3
"""
This script used to generate __manifest__ depends for project core install
"""

import ast
import json
import os
import sys
from functools import reduce
from importlib import import_module

sys.path.append(os.getcwd())

COLOR = {
    'red': '\033[1;31m',
    'green': '\033[1;32m',
    'yellow': '\033[1;33m',
    'off': '\033[1;m',
}
OPTIONS = """
Options:
    - project (project=biszx) *required
    - website (website=https://biszx.com)
        default:
    - version (version=14)
        default: 14
    - addon_path (addon_path=addons)
        default: addon
"""


def get_argv():
    """
    Make bash arguments into dict
    """
    argv_dict = {
        'addon_path': 'addons',
        'website': '',
        'version': '14.0',
    }
    for arg in sys.argv[1:]:
        pare = arg.split('=')
        key = pare[0]
        value = pare[1]
        argv_dict[key] = value
    return argv_dict


def validate_argv(argv):
    required_argv = ['project']
    return reduce(lambda a, v: a and v in argv, required_argv, True)


def get_manifest_path(module: str) -> str:
    manifest_path: str = None
    dirs: list = ['addons', 'additional-addons']
    for d in dirs:
        try_path: str = f'{d}/{module}/__manifest__.py'
        if os.path.exists(try_path):
            manifest_path = try_path
            break
    return manifest_path


def get_module_info(module: str) -> dict:
    manifest_path: str = get_manifest_path(module)
    if manifest_path:
        info: dict = {
            'depends': [],
            'external_dependencies': {},
        }
        with open(manifest_path) as f:
            info.update(ast.literal_eval(f.read()))
        return info
    return {}


def get_depends(argv):
    ext = argv.get('ext', {})
    addon_paths = [argv['addon_path']] + ext.get('addon_path', [])
    depends = []
    for addon_path in addon_paths:
        depends += list(
            filter(
                lambda v: (
                    os.path.isdir(os.path.join(addon_path, v))
                    and v != f'{argv["project"].lower()}_core_install'
                    and v not in ext.get('exclude_dirs', [])
                ),
                os.listdir(addon_path),
            )
        )
    depends += ext.get('depends', [])
    depends_set = set(depends)
    for module in depends:
        depends_set -= set(get_module_info(module).get('depends', []))
    depends = list(depends_set)
    depends.sort()
    return list(depends)


def generate_manifest(argv, manifest_path):
    project = argv['project'].upper()
    manifest_value = {
        'name': f'{project}: Core Install',
        'summary': f'{project} Core install (auto generate)',
        'author': argv.get('author', project),
        'website': argv['website'],
        'category': argv.get('category', 'Hidden'),
        'version': f'{argv["version"]}.1.0.0',
        'license': argv.get('license', 'LGPL-3'),
        'contributors': ['Biszx'],
        'depends': get_depends(argv),
    }
    content = (
        json.dumps(manifest_value, indent=4).replace("'", "\\'").replace('"', "'")
        + '\n'
    )
    manifest_exists = os.path.exists(manifest_path)
    exit_code = not manifest_exists
    if manifest_exists:
        with open(manifest_path) as f:
            exit_code = f.read() != content
    if exit_code:
        with open(manifest_path, 'w') as f:
            f.write(content)
    return int(exit_code)


def main():
    argv = get_argv()
    if validate_argv(argv):
        core_install_path = os.path.join(
            argv['addon_path'], '%s_core_install' % argv['project']
        )
        init_path = f'{core_install_path}/__init__.py'
        manifest_path = f'{core_install_path}/__manifest__.py'
        ext_path = f'{core_install_path}/ext.py'
        if not os.path.exists(core_install_path):
            os.makedirs(core_install_path)
            exit_code = 1
        if not os.path.exists(init_path):
            with open(init_path, 'w') as f:
                f.write('')
            exit_code = 1

        has_ext = os.path.exists(ext_path)
        if has_ext:
            argv['ext'] = import_module(
                ext_path.split('.')[0].replace('/', '.')
            ).options
        exit_code = generate_manifest(argv, manifest_path)
        if exit_code:
            print('{green}Core install has been updated.{off}'.format(**COLOR))
            exit(exit_code)
    else:
        print('{red}Some options is missing' + OPTIONS + '{off}'.format(**COLOR))
        exit(1)


if __name__ == '__main__':
    main()
